package vatbox_QA.vatbox_QA;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class config_reader {
	
	Properties pro;

	public config_reader()
	{
		try {
			File source = new File ("./config_reader");

			FileInputStream input = new FileInputStream(source);

			pro = new Properties();

			pro.load(input);

		} catch (Exception exp) {

			System.out.println("Exception is: ---"+exp.getMessage());
		} 
	}
	


	public String get_email() {
		String email = pro.getProperty("email");
		return email;
	}


	public String get_password() {
		String password = pro.getProperty("password");
		return password;
	}


	public String get_vatbox_expedite() {
		String vatbox_expedite = pro.getProperty("vatbox_expedite");
		return vatbox_expedite;
	}
	
	public String get_vatbox_P1() {
		String vatbox_P1 = pro.getProperty("vatbox_P1");
		return vatbox_P1;
	}
	
	public String get_vatbox_P2() {
		String vatbox_P2 = pro.getProperty("vatbox_P2");
		return vatbox_P2;
	}
	
	public String get_vatbox_P3() {
		String vatbox_P3 = pro.getProperty("vatbox_P3");
		return vatbox_P3;
	}
	
	public String get_vatbox_navigation() {
		String vatbox_navigation = pro.getProperty("vatbox_navigation");
		return vatbox_navigation;
	}
	
	public String get_vatbox_login_URL() {
		String vatbox_login_url = pro.getProperty("vatbox_login_url");
		return vatbox_login_url;
	}
	
	public int waitpagetoloadint() {
		String waitpagetoload = pro.getProperty("waitpagetoload");
		int waitpagetoloadint = Integer.parseInt(waitpagetoload);
		return waitpagetoloadint;
	}
	
}








	