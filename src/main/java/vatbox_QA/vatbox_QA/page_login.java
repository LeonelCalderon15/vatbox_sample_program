package vatbox_QA.vatbox_QA;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class page_login {

	WebDriver driver;
	 
    public page_login(WebDriver driver){ 
             this.driver=driver; 
    }	

   //Using FindBy for locating elements
    //@FindBy(how=How.XPATH, using="//input[@placeholder='Email']") WebElement txtbx_Email;
    @FindBy(how=How.XPATH, using="//input[@id='md-input-1']") WebElement txtbx_Email;
    @FindBy(how=How.XPATH, using="//input[@id='md-input-4']") WebElement txtbx_Pass;
    @FindBy(how=How.XPATH, using="//button[@class='mat-raised-button mat-primary'][contains(.,'LOGIN')]") WebElement btn_Login;
    @FindBy(how=How.XPATH, using="//input[@id='md-input-1']") WebElement field_Email;


    // This method is to set Email in the email text box of vatbox login page
    public void setEmail(String strEmail)
    {
    	txtbx_Email.sendKeys(strEmail);
    }
    
    public void clickEmailtextbox()
    {
    	field_Email.click();
    }
    
    public void clickPasswortextbox()
    {
    	txtbx_Pass.click();
    }
    		
    // This method is to set Password in the password text box of vatbox login page
    public void setPassword(String strPassword)
    {
    	txtbx_Pass.sendKeys(strPassword);
    }

    // This method is to click login button of vatbox login page
    public void clickOnLoginButton()
    {
    	btn_Login.click();
    }  
    

}



