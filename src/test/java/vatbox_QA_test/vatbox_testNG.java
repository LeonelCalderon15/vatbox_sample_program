package vatbox_QA_test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import vatbox_QA.vatbox_QA.config_reader;
import vatbox_QA.vatbox_QA.page_login;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import vatbox_QA.vatbox_QA.wait_class;

public class vatbox_testNG {


	 private WebDriver driver;
	 config_reader _config_reader = new config_reader();
	 @BeforeTest 
		public void launch_app() 
		   {	
			
			  WebDriverManager.chromedriver().setup();
		      driver = new ChromeDriver();
			  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);			  			  	    
		   }
	 
		@Test(priority = 0)
			public void vatbox_login_test() {
				driver.navigate().to(_config_reader.get_vatbox_login_URL());
				driver.manage().window().maximize();	
				page_login _page_login = PageFactory.initElements(driver, page_login.class);
				_page_login.clickEmailtextbox();		
				_page_login.setEmail(_config_reader.get_email());
				_page_login.clickPasswortextbox();
				_page_login.setPassword(_config_reader.get_password());
				_page_login.clickOnLoginButton();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		}
		@Test(priority = 1)
			public void navigate_to_Expedite() throws InterruptedException {
				driver.navigate().to(_config_reader.get_vatbox_expedite());
				Thread.sleep(_config_reader.waitpagetoloadint());	
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
			}
		
		@Test(priority = 2)
			public void navigate_to_P1() throws InterruptedException {
				driver.navigate().to(_config_reader.get_vatbox_P1());	
				Thread.sleep(_config_reader.waitpagetoloadint());
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		}
		
		@Test(priority = 3)
		public void navigate_to_P2() throws InterruptedException {
				driver.navigate().to(_config_reader.get_vatbox_P2());	
				Thread.sleep(_config_reader.waitpagetoloadint());
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		}
		
		@Test(priority = 4)
		public void navigate_to_P3() throws InterruptedException {
				driver.navigate().to(_config_reader.get_vatbox_P3());
				Thread.sleep(_config_reader.waitpagetoloadint());
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		}
		
		@Test(priority = 5)
		public void navigate_to_navigate() throws InterruptedException {
				driver.navigate().to(_config_reader.get_vatbox_navigation());
				Thread.sleep(_config_reader.waitpagetoloadint());
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		}
  
		@AfterTest
			public void closed_vatbox() {
			driver.close();
			}
}
